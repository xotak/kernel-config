# Kernel configs

This repository contains kernel configuration for my different devices running Gentoo. It contains Gentoo specific options and othe stuff that I probably don't need or needed and don't need anymore.

## Laptop

Brand : Lenovo  
Model : IdeaPad 3 15ADA05  
CPU : AMD Ryzen 5 3500U  
GPU : AMD Radeon Vega 8  

`lspci -k`:

```
00:00.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Root Complex
        Subsystem: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Root Complex
00:00.2 IOMMU: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 IOMMU
        Subsystem: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 IOMMU
00:01.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 00h-1fh) PCIe Dummy Host Bridge
00:01.1 PCI bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 PCIe GPP Bridge [6:0]
        Subsystem: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 PCIe GPP Bridge [6:0]
        Kernel driver in use: pcieport
00:01.2 PCI bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 PCIe GPP Bridge [6:0]
        Subsystem: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 PCIe GPP Bridge [6:0]
        Kernel driver in use: pcieport
00:08.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 00h-1fh) PCIe Dummy Host Bridge
00:08.1 PCI bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Internal PCIe GPP Bridge 0 to Bus A
        Subsystem: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Internal PCIe GPP Bridge 0 to Bus A
        Kernel driver in use: pcieport
00:14.0 SMBus: Advanced Micro Devices, Inc. [AMD] FCH SMBus Controller (rev 61)
        Subsystem: Lenovo FCH SMBus Controller
        Kernel driver in use: piix4_smbus
        Kernel modules: sp5100_tco
00:14.3 ISA bridge: Advanced Micro Devices, Inc. [AMD] FCH LPC Bridge (rev 51)
        Subsystem: Lenovo FCH LPC Bridge
00:18.0 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 0
00:18.1 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 1
00:18.2 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 2
00:18.3 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 3
        Kernel driver in use: k10temp
        Kernel modules: k10temp
00:18.4 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 4
00:18.5 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 5
00:18.6 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 6
00:18.7 Host bridge: Advanced Micro Devices, Inc. [AMD] Raven/Raven2 Device 24: Function 7
01:00.0 Non-Volatile memory controller: SK hynix BC511 NVMe SSD
        Subsystem: SK hynix BC511 NVMe SSD
        Kernel driver in use: nvme
02:00.0 Network controller: Realtek Semiconductor Co., Ltd. RTL8822CE 802.11ac PCIe Wireless Network Adapter
        Subsystem: Lenovo RTL8822CE 802.11ac PCIe Wireless Network Adapter
        Kernel driver in use: rtw_8822ce
        Kernel modules: rtw88_8822ce
03:00.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] Picasso/Raven 2 [Radeon Vega Series / Radeon Vega Mobile Series] (rev c2)
        Subsystem: Lenovo Picasso/Raven 2 [Radeon Vega Series / Radeon Vega Mobile Series]
        Kernel driver in use: amdgpu
        Kernel modules: amdgpu
03:00.1 Audio device: Advanced Micro Devices, Inc. [AMD/ATI] Raven/Raven2/Fenghuang HDMI/DP Audio Controller
        Subsystem: Lenovo Raven/Raven2/Fenghuang HDMI/DP Audio Controller
        Kernel driver in use: snd_hda_intel
        Kernel modules: snd_hda_intel
03:00.2 Encryption controller: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 10h-1fh) Platform Security Processor
        Subsystem: Lenovo Family 17h (Models 10h-1fh) Platform Security Processor
        Kernel driver in use: ccp
        Kernel modules: ccp
03:00.3 USB controller: Advanced Micro Devices, Inc. [AMD] Raven USB 3.1
        Subsystem: Lenovo Raven USB 3.1
        Kernel driver in use: xhci_hcd
03:00.4 USB controller: Advanced Micro Devices, Inc. [AMD] Raven USB 3.1
        Subsystem: Lenovo Raven USB 3.1
        Kernel driver in use: xhci_hcd
03:00.5 Multimedia controller: Advanced Micro Devices, Inc. [AMD] ACP/ACP3X/ACP6x Audio Coprocessor
        Subsystem: Lenovo ACP/ACP3X/ACP6x Audio Coprocessor
        Kernel driver in use: snd_pci_acp3x
        Kernel modules: snd_pci_acp3x, snd_rn_pci_acp3x, snd_pci_acp5x, snd_pci_acp6x, snd_acp_pci, snd_rpl_pci_acp6x, snd_pci_ps, snd_sof_amd_renoir, snd_sof_amd_rembrandt
03:00.6 Audio device: Advanced Micro Devices, Inc. [AMD] Family 17h/19h HD Audio Controller
        Subsystem: Lenovo Family 17h/19h HD Audio Controller
        Kernel driver in use: snd_hda_intel
        Kernel modules: snd_hda_intel
```

Kernel configuration file : laptop-config

That config contain stuff that I enabled, didn't work, use or not. Includes proper configuration like docker support, kvm amd virtualization, tried to run waydroid too but didn't work ~~(skillissue)~~

Kernel configuration file : laptop-config-clean

The bare minimum kernel configuration required, with all pc's hardware working, including touchpad (yay). The encryption controller is recognized but no kernel module used. It's probably have been built-in. There's also some optimisation done iirc
